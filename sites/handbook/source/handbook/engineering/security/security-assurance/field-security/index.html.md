---
layout: handbook-page-toc
title: "Field Security Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}
 
## Field Security Mission 
 
The Field Security team serves as the public representation of GitLab's internal Security function. The team is tasked with providing high levels of security assurance to internal and external customers. We work with all GitLab departments to document requests, analyze the risks associated with those requests, and provide value-added remediation recommendations.

Hear more about our team and how we support GitLab in this [interview with the Manager of Field Security.](https://www.youtube.com/watch?v=h95ddzEsTog). 
 
## Field Security Primary Functions

### Field Security
In support of GitLab’s Sales and Customer Success Teams, the Field Security team maintains the [Customer Assurance Activities Procedure](/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html) for the intake, tracking, and responding to GitLab Customer and Prospect Security Assurance Activity request. This includes, but is not limited to: 
 
- Proactively maintaining self-service security and privacy resrouces including the [Customer Assurance Package](https://about.gitlab.com/security/cap/), the [Trust Center](https://about.gitlab.com/security/) and the **internal only** [Answer Base](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/answerbase.html).
- Completing Security, Privacy, and Risk Management Questionnaires
- Assisting in Contract Reviews
- Participating in Customer Meetings
- Providing External Evidence (such as GitLab’s SOC2 Penetration Test reports)
- Conducting Annual [Field Security Study](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/field-security-study.html)
 
Requests for Customer Assurance Activities should be submitted using the **Customer Assurance** workflow in the `#sec-fieldsecurity` Slack Channel. Detailed work instructions are located in the [Field Security Project](https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/field-security#customer-assurance-activities). 
 
<!-- blank line -->
----
<!-- blank line -->

## Metrics and Measures of Success

[Security Impact on Net ARR](https://about.gitlab.com/handbook/engineering/security/performance-indicators/#security-impact-on-net-arr)
 
## Makeup of the team
- [Marie-Claire Cerny](https://about.gitlab.com/company/team/#marieclairecerny) @marieclairecerny 
- [Meghan Maneval](https://about.gitlab.com/company/team/?department=security-department#mmaneval20) @mmaneval20
- [Julia Lake](https://about.gitlab.com/company/team/#julia.lake) @julia.lake
 
## References
* [Customer Assurance Activities Procedure](/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html)
 
## Contact the Field Security Team

Do you have an idea, feedback, or recommendation for how Field Security can better support you? Pleaes open a General Request issue in the [Field Security Project.](https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/field-security) 

* GitLab Issues
  * `/gitlab-com/gl-security/security-assurance/field-security-team`
* Email
  * `fieldsecurity@gitlab.com`
* Slack
  * Group Handle
    * `@field-security`
  * Primary Slack Channels
    * `#sec-fieldsecurity`
    * `#sec-assurance`

<div class="flex-row" markdown="0" style="height:40px">
    <a href="https://about.gitlab.com/handbook/engineering/security/security-assurance/security-assurance.html#" class="btn btn-purple-inv" style="width:100%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Return to the Security Assurance Homepage</a>
</div> 
